<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mainform))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.checkBx = New System.Windows.Forms.CheckBox()
        Me.btnLanjutkan = New Hbd_bulan.ButtonBlue()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label1.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(79, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(412, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Hallo Bulan 2, Happy Birthdayy ^_^"
        Me.Label1.UseCompatibleTextRendering = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Location = New System.Drawing.Point(16, 307)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(534, 93)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Saya mempunyai tantangan quis untuk kamu" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "apakah bersedia untuk menjawabnya ?"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(153, 66)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(257, 219)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'checkBx
        '
        Me.checkBx.AutoSize = True
        Me.checkBx.Cursor = System.Windows.Forms.Cursors.Hand
        Me.checkBx.Font = New System.Drawing.Font("Consolas", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkBx.ForeColor = System.Drawing.Color.Blue
        Me.checkBx.Location = New System.Drawing.Point(20, 403)
        Me.checkBx.Name = "checkBx"
        Me.checkBx.Size = New System.Drawing.Size(259, 21)
        Me.checkBx.TabIndex = 3
        Me.checkBx.Text = "Ya, Saya bersedia bapack-bapack!"
        Me.checkBx.UseCompatibleTextRendering = True
        Me.checkBx.UseVisualStyleBackColor = True
        '
        'btnLanjutkan
        '
        Me.btnLanjutkan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLanjutkan.Enabled = False
        Me.btnLanjutkan.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.btnLanjutkan.Image = Nothing
        Me.btnLanjutkan.Location = New System.Drawing.Point(420, 408)
        Me.btnLanjutkan.Name = "btnLanjutkan"
        Me.btnLanjutkan.NoRounding = False
        Me.btnLanjutkan.Size = New System.Drawing.Size(130, 36)
        Me.btnLanjutkan.TabIndex = 4
        Me.btnLanjutkan.Text = "Lanjutkan>>"
        '
        'mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GreenYellow
        Me.ClientSize = New System.Drawing.Size(562, 451)
        Me.Controls.Add(Me.btnLanjutkan)
        Me.Controls.Add(Me.checkBx)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "mainform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HBD Bulan 2"
        Me.TopMost = True
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents checkBx As System.Windows.Forms.CheckBox
    Friend WithEvents btnLanjutkan As Hbd_bulan.ButtonBlue

End Class
