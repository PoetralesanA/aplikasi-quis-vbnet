﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class p1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(p1))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.A = New System.Windows.Forms.RadioButton()
        Me.B = New System.Windows.Forms.RadioButton()
        Me.C = New System.Windows.Forms.RadioButton()
        Me.D = New System.Windows.Forms.RadioButton()
        Me.ButtonGreen1 = New Hbd_bulan.ButtonGreen()
        Me.PanelBox1 = New Hbd_bulan.PanelBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Maroon
        Me.Label2.Location = New System.Drawing.Point(0, -1)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(709, 107)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "LEVEL 1"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Consolas", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(54, 138)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(600, 32)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Apakah makanan kaporit dari pak Andi..?"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(99, 260)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(35, 21)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'A
        '
        Me.A.AutoSize = True
        Me.A.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold)
        Me.A.ForeColor = System.Drawing.Color.White
        Me.A.Location = New System.Drawing.Point(60, 185)
        Me.A.Name = "A"
        Me.A.Size = New System.Drawing.Size(116, 19)
        Me.A.TabIndex = 9
        Me.A.TabStop = True
        Me.A.Text = "A. Tidakmakan"
        Me.A.UseVisualStyleBackColor = True
        '
        'B
        '
        Me.B.AutoSize = True
        Me.B.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold)
        Me.B.ForeColor = System.Drawing.Color.White
        Me.B.Location = New System.Drawing.Point(60, 210)
        Me.B.Name = "B"
        Me.B.Size = New System.Drawing.Size(109, 19)
        Me.B.TabIndex = 10
        Me.B.TabStop = True
        Me.B.Text = "B. Pangduduk"
        Me.B.UseVisualStyleBackColor = True
        '
        'C
        '
        Me.C.AutoSize = True
        Me.C.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold)
        Me.C.ForeColor = System.Drawing.Color.White
        Me.C.Location = New System.Drawing.Point(60, 235)
        Me.C.Name = "C"
        Me.C.Size = New System.Drawing.Size(249, 19)
        Me.C.TabIndex = 11
        Me.C.TabStop = True
        Me.C.Text = "C. Makan semua yang didepan mata"
        Me.C.UseVisualStyleBackColor = True
        '
        'D
        '
        Me.D.AutoSize = True
        Me.D.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold)
        Me.D.ForeColor = System.Drawing.Color.White
        Me.D.Location = New System.Drawing.Point(60, 260)
        Me.D.Name = "D"
        Me.D.Size = New System.Drawing.Size(102, 19)
        Me.D.TabIndex = 12
        Me.D.TabStop = True
        Me.D.Text = "D.      Mie"
        Me.D.UseVisualStyleBackColor = True
        '
        'ButtonGreen1
        '
        Me.ButtonGreen1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonGreen1.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.ButtonGreen1.Image = Nothing
        Me.ButtonGreen1.Location = New System.Drawing.Point(504, 309)
        Me.ButtonGreen1.Name = "ButtonGreen1"
        Me.ButtonGreen1.NoRounding = False
        Me.ButtonGreen1.Size = New System.Drawing.Size(192, 48)
        Me.ButtonGreen1.TabIndex = 15
        Me.ButtonGreen1.Text = "Next>>"
        '
        'PanelBox1
        '
        Me.PanelBox1.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.PanelBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.PanelBox1.Location = New System.Drawing.Point(42, 294)
        Me.PanelBox1.Name = "PanelBox1"
        Me.PanelBox1.NoRounding = False
        Me.PanelBox1.Size = New System.Drawing.Size(664, 9)
        Me.PanelBox1.TabIndex = 14
        Me.PanelBox1.Text = "PanelBox1"
        '
        'p_1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Purple
        Me.ClientSize = New System.Drawing.Size(708, 367)
        Me.Controls.Add(Me.ButtonGreen1)
        Me.Controls.Add(Me.PanelBox1)
        Me.Controls.Add(Me.C)
        Me.Controls.Add(Me.B)
        Me.Controls.Add(Me.A)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.D)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "p_1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HBD bulan 2 | Pertanyaan 1"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents A As System.Windows.Forms.RadioButton
    Friend WithEvents B As System.Windows.Forms.RadioButton
    Friend WithEvents C As System.Windows.Forms.RadioButton
    Friend WithEvents D As System.Windows.Forms.RadioButton
    Friend WithEvents PanelBox1 As Hbd_bulan.PanelBox
    Friend WithEvents ButtonGreen1 As Hbd_bulan.ButtonGreen
End Class
