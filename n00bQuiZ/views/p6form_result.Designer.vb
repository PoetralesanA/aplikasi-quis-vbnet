﻿'<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
'Partial Class finalform
'    Inherits System.Windows.Forms.Form

'    'Form overrides dispose to clean up the component list.
'    <System.Diagnostics.DebuggerNonUserCode()> _
'    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
'        Try
'            If disposing AndAlso components IsNot Nothing Then
'                components.Dispose()
'            End If
'        Finally
'            MyBase.Dispose(disposing)
'        End Try
'    End Sub

'    'Required by the Windows Form Designer
'    Private components As System.ComponentModel.IContainer

'    'NOTE: The following procedure is required by the Windows Form Designer
'    'It can be modified using the Windows Form Designer.  
'    'Do not modify it using the code editor.
'    <System.Diagnostics.DebuggerStepThrough()> _
'    Private Sub InitializeComponent()
'        components = New System.ComponentModel.Container
'        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
'        Me.Text = "finalform"
'    End Sub
'End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class p6form_result
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.label_Score = New System.Windows.Forms.Label()
        Me.labelBenar = New System.Windows.Forms.Label()
        Me.labelSalah = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PanelBox1 = New Hbd_bulan.PanelBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.labelResult = New System.Windows.Forms.Label()
        Me.ButtonGreen1 = New Hbd_bulan.ButtonGreen()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.MidnightBlue
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label2.Location = New System.Drawing.Point(0, -1)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(709, 107)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "[ ~ ~ HASIL ~ ~ ]"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label_Score
        '
        Me.label_Score.Font = New System.Drawing.Font("Consolas", 18.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_Score.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.label_Score.Location = New System.Drawing.Point(3, 170)
        Me.label_Score.Name = "label_Score"
        Me.label_Score.Size = New System.Drawing.Size(639, 32)
        Me.label_Score.TabIndex = 3
        Me.label_Score.Text = "Score : "
        '
        'labelBenar
        '
        Me.labelBenar.Font = New System.Drawing.Font("Consolas", 18.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelBenar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.labelBenar.Location = New System.Drawing.Point(3, 14)
        Me.labelBenar.Name = "labelBenar"
        Me.labelBenar.Size = New System.Drawing.Size(639, 32)
        Me.labelBenar.TabIndex = 16
        Me.labelBenar.Text = "Benar : "
        '
        'labelSalah
        '
        Me.labelSalah.Font = New System.Drawing.Font("Consolas", 18.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelSalah.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.labelSalah.Location = New System.Drawing.Point(3, 65)
        Me.labelSalah.Name = "labelSalah"
        Me.labelSalah.Size = New System.Drawing.Size(639, 32)
        Me.labelSalah.TabIndex = 17
        Me.labelSalah.Text = "Salah : "
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.MediumPurple
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.labelSalah)
        Me.Panel1.Controls.Add(Me.labelBenar)
        Me.Panel1.Controls.Add(Me.PanelBox1)
        Me.Panel1.Controls.Add(Me.label_Score)
        Me.Panel1.Location = New System.Drawing.Point(17, 354)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(675, 219)
        Me.Panel1.TabIndex = 18
        '
        'PanelBox1
        '
        Me.PanelBox1.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.PanelBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.PanelBox1.Location = New System.Drawing.Point(3, 143)
        Me.PanelBox1.Name = "PanelBox1"
        Me.PanelBox1.NoRounding = False
        Me.PanelBox1.Size = New System.Drawing.Size(664, 9)
        Me.PanelBox1.TabIndex = 14
        Me.PanelBox1.Text = "PanelBox1"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(208, 109)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(289, 207)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 19
        Me.PictureBox1.TabStop = False
        '
        'labelResult
        '
        Me.labelResult.Font = New System.Drawing.Font("Cooper Black", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelResult.ForeColor = System.Drawing.Color.Lime
        Me.labelResult.Location = New System.Drawing.Point(2, 319)
        Me.labelResult.Name = "labelResult"
        Me.labelResult.Size = New System.Drawing.Size(705, 32)
        Me.labelResult.TabIndex = 20
        Me.labelResult.Text = "getResult()"
        Me.labelResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonGreen1
        '
        Me.ButtonGreen1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonGreen1.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.ButtonGreen1.Image = Nothing
        Me.ButtonGreen1.Location = New System.Drawing.Point(560, 589)
        Me.ButtonGreen1.Name = "ButtonGreen1"
        Me.ButtonGreen1.NoRounding = False
        Me.ButtonGreen1.Size = New System.Drawing.Size(132, 32)
        Me.ButtonGreen1.TabIndex = 15
        Me.ButtonGreen1.Text = "Mulai Lagi?"
        '
        'finalform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Purple
        Me.ClientSize = New System.Drawing.Size(708, 633)
        Me.Controls.Add(Me.labelResult)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ButtonGreen1)
        Me.Controls.Add(Me.Label2)
        Me.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "finalform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HBD bulan 2 | RESULT"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents label_Score As System.Windows.Forms.Label
    Friend WithEvents PanelBox1 As Hbd_bulan.PanelBox
    Friend WithEvents ButtonGreen1 As Hbd_bulan.ButtonGreen
    Friend WithEvents labelBenar As System.Windows.Forms.Label
    Friend WithEvents labelSalah As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents labelResult As System.Windows.Forms.Label
End Class
