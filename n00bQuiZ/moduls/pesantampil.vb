﻿Namespace tampilkanpesan
    Module type
        Public Sub information(ByVal namaform As Form, ByVal caption As String, ByVal title As String)
            MessageBox.Show(namaform, caption, title, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Sub
        Public Sub Exclamation(ByVal namaform As Form, ByVal caption As String, ByVal title As String)
            MessageBox.Show(namaform, caption, title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Sub

        Public Sub ErrorCuk(ByVal namaform As Form, ByVal caption As String, ByVal title As String)
            MessageBox.Show(namaform, caption, title, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Sub
    End Module
End Namespace
Namespace getImage
    Module gif
        Public Sub mypicturesname(ByVal p As PictureBox, ByVal rsource As Object)
            p.Image = rsource
        End Sub
    End Module
End Namespace