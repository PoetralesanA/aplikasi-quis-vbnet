Module methodQuis
    Private var_score As Integer
    Private var_jawabanbenar As Integer
    Private var_jawabansalah As Integer

    Class hasiljawaban
        Public Function benar()
            Return var_jawabanbenar
        End Function
        Public Function salah()
            Return var_jawabansalah
        End Function
        Public Function score()
            Return var_score
        End Function
    End Class

    Private Function hitungscore()
        var_score += 20
        Return var_jawabanbenar
    End Function
    Public Function hitung_jawaban_benar() As Integer
        var_jawabanbenar += 1
        hitungscore()
        Return var_jawabanbenar
    End Function
    Public Function hitung_jawaban_salah() As Integer
        var_jawabansalah += 1
        Return var_jawabansalah
    End Function

    'pemilihan radiobutton
    Public Sub jawabansaya(ByVal memilihRadiobutton As RadioButton)
        Select Case memilihRadiobutton.Checked
            Case True
                hitung_jawaban_benar()
            Case Else
                hitung_jawaban_salah()
        End Select
    End Sub
#Region "Pesan Tampil"
    Public Sub quizpesanresult()
        If var_score <= 40 Then
            tampilkanpesan.type.Exclamation(p6form_result, "Kalian kurang memahami satusama lainya" & vbNewLine & "Tingkatkan lagi yaa kedekatanya.. ^_^",
                                            "Yahhh nilainya kurang nih :(")
        Else
            tampilkanpesan.type.information(p6form_result, "Wow, . kalian saling kenal ternyata yah" & vbNewLine & "Gak sia-sia Earphone ditelinga mulu, ckck.",
                                        "Watashi wa, anata o aishiteimasu^^")
        End If
    End Sub
#End Region
End Module